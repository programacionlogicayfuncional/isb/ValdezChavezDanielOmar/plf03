(ns plf03.core)

(defn función-comp-1
  []
  (let [f (fn [x] (* 3 x)) 
        g (fn [x] (* 2 x))
        
        z (comp f g)]
    
    (z 10)))

(defn función-comp-2
  []
  (let [f (fn [x] (* 3 x)) 
        g (fn [x] (- 2 x))
        
        z(comp f g)]
    
    (z 23)))

(defn función-comp-3
  []
  (let [f (fn [x] (* 3 x)) 
        g (fn [x] (+ 3 x))
        
        z(comp f g)]
    
    (z 12)))

(defn función-comp-4
  []
  (let [f (fn [x] (* 3 x)) 
        g (fn [x] (/ 2 x))
        
        z(comp f g)]
    
    (z 34)))

(defn función-comp-5
  []
  (let [f (fn [x] (/ 3 x)) 
        g (fn [x] (* 2 x))
        
        z(comp f g)]
    
    (z 45)))

(defn función-comp-6
  []
  (let [f (fn [x] (* 3 x)) 
        g (fn [x] (inc x))
        
        z(comp f g)]
    
    (z 56)))

(defn función-comp-7
  []
  (let [f (fn [x] (inc x)) 
        g (fn [x] (inc x)) 
        h (fn [x] (inc x)) 
        i (fn [x] (inc x))
        
        z(comp f g h i)]
    
    (z 98)))

(defn función-comp-8
  []
  (let [f (fn [x] (inc x)) 
        g (fn [x] (inc x)) 
        h (fn [x] (dec x))
        
        z(comp f g h)]
    
    (z 44)))

(defn función-comp-9
  []
  (let [f (fn [x] (* 2 x)) 
        g (fn [x] (inc x))
        
        z(comp f g)]
    
    (z 67)))

(defn función-comp-10
  []
  (let [f (fn [x] (* 2 x)) 
        g (fn [x] (mod x 2))
        
        z(comp f g)]
    
    (z 11)))

(defn función-comp-11
  []
  (let [f (fn [x] (mod x 3)) 
        g (fn [x] (mod x 2))
        
        z(comp f g)]
    
    (z 3434)))

(defn función-comp-12
  []
  (let [f (fn [x] (max x 3)) 
        g (fn [x] (mod x 2))
        
        z(comp f g)]
    
    (z 567)))

(defn función-comp-13
  []
  (let [f (fn [x] (min x 3)) 
        g (fn [x] (mod x 2))
        
        z(comp f g)]
    
    (z 123)))

(defn función-comp-14
  []
  (let [f (fn [x] (mod x 3)) 
        g (fn [x] (quot x 2))
        
        z(comp f g)]
    
    (z 345)))

(defn función-comp-15
  []
  (let [f (fn [x] (rem x 3)) 
        g (fn [x] (quot x 2))
        
        z(comp f g)]
    
    (z 5677)))

(defn función-comp-16
  []
  (let [f (fn [x] (quot x 3)) 
        g (fn [x] (rem x 2))
        
        z(comp f g)]
    
    (z 3412)))

(defn función-comp-17
  []
  (let [f (fn [x] (quot x 3)) 
        g (fn [x] (rem x 2)) 
        h (fn [x] (mod x 2))
        
        z(comp f g h)]
    
    (z 45456)))

(defn función-comp-18
  []
  (let [f (fn [x] (rem x 2)) 
        g (fn [x] (- x 2)) 
        h (fn [x] (quot x 2)) 
        i (fn [x] (inc x))
        
        z(comp f g h i)]
    
    (z 1398)))

(defn función-comp-19
  []
  (let [f (fn [x] (rem x 3)) 
        g (fn [x] (min x 5)) 
        h (fn [x] (max x 2)) 
        i (fn [x] (rem x 2)) 
        j (fn [x] (quot x 2))
        
        z(comp f g h i j)]
    
    (z 987978)))

(defn función-comp-20
  []
  (let [f (fn [x] (str x)) 
        g (fn [x] (rem x 2))
        
        z(comp f g)]
    
    (z 876876)))

(función-comp-1)
(función-comp-2)
(función-comp-3)
(función-comp-4)
(función-comp-5)
(función-comp-6)
(función-comp-7)
(función-comp-8)
(función-comp-9)
(función-comp-10)
(función-comp-11)
(función-comp-12)
(función-comp-13)
(función-comp-14)
(función-comp-15)
(función-comp-16)
(función-comp-17)
(función-comp-18)
(función-comp-19)
(función-comp-20)

(defn función-complement-1
  []
  (let [f (fn [x] (pos? x))
        
        z (complement f)]
    
    (z 12)))

(defn función-complement-2
  []
  (let [f (fn [x] (pos-int? x))
        
        z(complement f)]
    
    (z 435345)))

(defn función-complement-3
  []
  (let [f (fn [x] (neg? x))
        
        z(complement f)]
    
    (z 567)))

(defn función-complement-4
  []
  (let [f (fn [x] (neg-int? x))
       
        z (complement f)]
    
    (z 123)))

(defn función-complement-5
  []
  (let [f (fn [x] (float? x))
        
        z(complement f)]
    
    (z 567)))

(defn función-complement-6
  []
  (let [f (fn [x] (ratio? x))
        
        z(complement f)]
    
    (z 777)))

(defn función-complement-7
  []
  (let [f (fn [x] (rational? x))
        
        z(complement f)]
    
    (z 111)))

(defn función-complement-8
  []
  (let [f (fn [x] (double? x))
        
        z(complement f)]
    
    (z 222)))

(defn función-complement-9
  []
  (let [f (fn [x] (decimal? x))
        
        z(complement f)]
    
    (z 333)))

(defn función-complement-10
  []
  (let [f (fn [x] (number? x))
        
        z(complement f)]
    
    (z "a")))

(defn función-complement-11
  []
  (let [f (fn [x] (odd? x))
        
        z(complement f)]
    
    (z 561)))

(defn función-complement-12
  []
  (let [f (fn [x] (even? x))
        
        z(complement f)]
    
    (z 555)))

(defn función-complement-13
  []
  (let [f (fn [x] (true? x))
        
        z(complement f)]
    
    (z false)))

(defn función-complement-14
  []
  (let [f (fn [x] (false? x))
        
        z(complement f)]
    
    (z false)))

(defn función-complement-15
  []
  (let [f (fn [x] (nil? x))
        
        z(complement f)]
    
    (z nil)))

(defn función-complement-16
  []
  (let [f (fn [x] (some? x))
        
        z(complement f)]
    
    (z [nil])))

(defn función-complement-17
  []
  (let [f (fn [x] (symbol? x))
        
        z(complement f)]
    
    (z :a)))

(defn función-complement-18
  []
  (let [f (fn [x] (list? x))
        
        z(complement f)]
    
    (z {:a 23})))

(defn función-complement-19
  []
  (let [f (fn [x] (vector? x))
        
        z(complement f)]
    
    (z [23])))

(defn función-complement-20
  []
  (let [f (fn [x] (coll? x))
        
        z(complement f)]
    
    (z 234)))

(función-complement-1)
(función-complement-2)
(función-complement-3)
(función-complement-4)
(función-complement-5)
(función-complement-6)
(función-complement-7)
(función-complement-8)
(función-complement-9)
(función-complement-10)
(función-complement-11)
(función-complement-12)
(función-complement-13)
(función-complement-14)
(función-complement-15)
(función-complement-16)
(función-complement-17)
(función-complement-18)
(función-complement-19)
(función-complement-20)

(defn función-constantly-1
  []
  (let [f (fn [x] (+ 2 x))
        z (constantly f)]
    (z 10)))

(defn función-constantly-2
  []
  (let [f (fn [x] (* 2 x))
        z (constantly f)]
    (z 10)))

(defn función-constantly-3
  []
  (let [f (fn [x] (/ 2 x))
        z (constantly f)]
    (z 10)))

(defn función-constantly-4
  []
  (let [f (fn [x] (- 2 x))
        z (constantly f)]
    (z 10)))

(defn función-constantly-5
  []
  (let [f (fn [x] (coll? x))
        z (constantly f)]
    (z 10)))

(defn función-constantly-6
  []
  (let [f (fn [x] (vector? x))
        z (constantly f)]
    (z 10)))

(defn función-constantly-7
  []
  (let [f (fn [x] (list? x))
        z (constantly f)]
    (z 10)))

(defn función-constantly-8
  []
  (let [f (fn [x] (inc x))
        z (constantly f)]
    (z 10)))

(defn función-constantly-9
  []
  (let [f (fn [x] (dec x))
        z (constantly f)]
    (z 10)))

(defn función-constantly-10
  []
  (let [f (fn [x] (pos? x))
        z (constantly f)]
    (z 10)))

(defn función-constantly-11
  []
  (let [f (fn [x] (neg? x))
        z (constantly f)]
    (z 10)))

(defn función-constantly-12
  []
  (let [f (fn [x] (mod x 2))
        z (constantly f)]
    (z 10)))

(defn función-constantly-13
  []
  (let [f (fn [x] (quot x 2))
        z (constantly f)]
    (z 10)))

(defn función-constantly-14
  []
  (let [f (fn [x] (rem 2 x))
        z (constantly f)]
    (z 10)))

(defn función-constantly-15
  []
  (let [f (fn [x] (str x))
        z (constantly f)]
    (z 10)))

(defn función-constantly-16
  []
  (let [f (fn [x] (symbol? x))
        z (constantly f)]
    (z 10)))

(defn función-constantly-17
  []
  (let [f (fn [x] (char? x))
        z (constantly f)]
    (z 10)))

(defn función-constantly-18
  []
  (let [f (fn [x] (number? x))
        z (constantly f)]
    (z 10)))

(defn función-constantly-19
  []
  (let [f (fn [x] (boolean? x))
        z (constantly f)]
    (z 10)))

(defn función-constantly-20
  []
  (let [f (fn [x y] (* x y))
        z (constantly f)]
    (z 10 20)))

(función-constantly-1)
(función-constantly-2)
(función-constantly-3)
(función-constantly-4)
(función-constantly-5)
(función-constantly-6)
(función-constantly-7)
(función-constantly-8)
(función-constantly-9)
(función-constantly-10)
(función-constantly-11)
(función-constantly-12)
(función-constantly-13)
(función-constantly-14)
(función-constantly-15)
(función-constantly-16)
(función-constantly-17)
(función-constantly-18)
(función-constantly-19)
(función-constantly-20)

(defn función-every-pred-1
  []
  (let [z (every-pred boolean?)]
    (z 23)))

(defn función-every-pred-2
  []
  (let [z (every-pred number? pos?)]
    (z 23)))

(defn función-every-pred-3
  []
  (let [z (every-pred number? neg?)]
    (z -23)))

(defn función-every-pred-4
  []
  (let [z (every-pred number? pos-int?)]
    (z 123)))

(defn función-every-pred-5
  []
  (let [z (every-pred number? neg-int?)]
    (z 22123)))

(defn función-every-pred-6
  []
  (let [z (every-pred number? ratio?)]
    (z 23/6)))

(defn función-every-pred-7
  []
  (let [z (every-pred number? rational?)]
    (z 23/7)))

(defn función-every-pred-8
  []
  (let [z (every-pred number? float?)]
    (z 23.2323)))

(defn función-every-pred-9
  []
  (let [z (every-pred number? double?)]
    (z 23.5)))

(defn función-every-pred-10
  []
  (let [z (every-pred char? symbol?)]
    (z "23")))

(defn función-every-pred-11
  []
  (let [z (every-pred char? string?)]
    (z 555)))

(defn función-every-pred-12
  []
  (let [z (every-pred coll? list?)]
    (z [23])))

(defn función-every-pred-13
  []
  (let [z (every-pred coll? map?)]
    (z {:a 23})))

(defn función-every-pred-14
  []
  (let [z (every-pred coll? vector?)]
    (z [23])))

(defn función-every-pred-15
  []
  (let [z (every-pred coll? map-entry?)]
    (z {:a 23})))

(defn función-every-pred-16
  []
  (let [z (every-pred coll? some?)]
    (z [])))

(defn función-every-pred-17
  []
  (let [z (every-pred coll? set?)]
    (z '(23))))

(defn función-every-pred-18
  []
  (let [z (every-pred coll? associative?)]
    (z [23])))

(defn función-every-pred-19
  []
  (let [z (every-pred coll? sequential?)]
    (z [23 24 25])))

(defn función-every-pred-20
  []
  (let [z (every-pred coll? seq?)]
    (z [23 24 25])))

(función-every-pred-1)
(función-every-pred-2)
(función-every-pred-3)
(función-every-pred-4)
(función-every-pred-5)
(función-every-pred-6)
(función-every-pred-7)
(función-every-pred-8)
(función-every-pred-9)
(función-every-pred-10)
(función-every-pred-11)
(función-every-pred-12)
(función-every-pred-13)
(función-every-pred-14)
(función-every-pred-15)
(función-every-pred-16)
(función-every-pred-17)
(función-every-pred-18)
(función-every-pred-19)
(función-every-pred-20)

(defn función-fnil-1
  []
  (let [f (fn[x] (+ 2 x))
        
        z (fnil f 23)]
    (z nil)))

(defn función-fnil-2
  []
  (let [f (fn [x] (* 2 x))

        z (fnil f 1)]
    (z nil)))

(defn función-fnil-3
  []
  (let [f (fn [x] (* x x))

        z (fnil f 1)]
    (z nil)))

(defn función-fnil-4
  []
  (let [f (fn [x] (- 2 x))

        z (fnil f 0)]
    (z nil)))

(defn función-fnil-5
  []
  (let [f (fn [x] (/ x 3))

        z (fnil f 1)]
    (z nil)))

(defn función-fnil-6
  []
  (let [f (fn [x] (boolean? x))

        z (fnil f 1)]
    (z nil)))

(defn función-fnil-7
  []
  (let [f (fn [x] (char? x))

        z (fnil f "1")]
    (z nil)))

(defn función-fnil-8
  []
  (let [f (fn [x] (pos? x))

        z (fnil f -1)]
    (z nil)))

(defn función-fnil-9
  []
  (let [f (fn [x] (neg? x))

        z (fnil f 1)]
    (z nil)))

(defn función-fnil-10
  []
  (let [f (fn [x] (inc x))

        z (fnil f 1)]
    (z nil)))

(defn función-fnil-11
  []
  (let [f (fn [x] (dec x))

        z (fnil f 2)]
    (z nil)))

(defn función-fnil-12
  []
  (let [f (fn [x] (string? x))

        z (fnil f 1)]
    (z nil)))

(defn función-fnil-13
  []
  (let [f (fn [x] (float? x))

        z (fnil f 1)]
    (z nil)))

(defn función-fnil-14
  []
  (let [f (fn [x] (str x))

        z (fnil f 1)]
    (z nil)))

(defn función-fnil-15
  []
  (let [f (fn [x] (coll? x))

        z (fnil f [1])]
    (z nil)))

(defn función-fnil-16
  []
  (let [f (fn [x] (vector? x))

        z (fnil f {:a 1})]
    (z nil)))

(defn función-fnil-17
  []
  (let [f (fn [x] (list? x))

        z (fnil f [1])]
    (z nil)))

(defn función-fnil-18
  []
  (let [f (fn [x y] (vector x y))

        z (fnil f 1 2)]
    (z nil nil)))

(defn función-fnil-19
  []
  (let [f (fn [x] (map? x))

        z (fnil f {:a 3})]
    (z nil)))

(defn función-fnil-20
  []
  (let [f (fn [x] (/ 2 x))

        z (fnil f 1)]
    (z nil)))


(función-fnil-1)
(función-fnil-2)
(función-fnil-3)
(función-fnil-4)
(función-fnil-5)
(función-fnil-6)
(función-fnil-7)
(función-fnil-8)
(función-fnil-9)
(función-fnil-10)
(función-fnil-11)
(función-fnil-12)
(función-fnil-13)
(función-fnil-14)
(función-fnil-15)
(función-fnil-16)
(función-fnil-17)
(función-fnil-18)
(función-fnil-19)
(función-fnil-20)


(defn función-juxt-1
  []
  (let [f (fn [x] (* 3 x))
        g (fn [x] (* 2 x))

        z (juxt f g)]

    (z 10)))

(defn función-juxt-2
  []
  (let [f (fn [x] (* 3 x))
        g (fn [x] (- 2 x))

        z (juxt f g)]

    (z 23)))

(defn función-juxt-3
  []
  (let [f (fn [x] (* 3 x))
        g (fn [x] (+ 3 x))

        z (juxt f g)]

    (z 12)))

(defn función-juxt-4
  []
  (let [f (fn [x] (* 3 x))
        g (fn [x] (/ 2 x))

        z (juxt f g)]

    (z 34)))

(defn función-juxt-5
  []
  (let [f (fn [x] (/ 3 x))
        g (fn [x] (* 2 x))

        z (juxt f g)]

    (z 45)))

(defn función-juxt-6
  []
  (let [f (fn [x] (* 3 x))
        g (fn [x] (inc x))

        z (juxt f g)]

    (z 56)))

(defn función-juxt-7
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (inc x))
        h (fn [x] (inc x))
        i (fn [x] (inc x))

        z (juxt f g h i)]

    (z 98)))

(defn función-juxt-8
  []
  (let [f (fn [x] (inc x))
        g (fn [x] (inc x))
        h (fn [x] (dec x))

        z (juxt f g h)]

    (z 44)))

(defn función-juxt-9
  []
  (let [f (fn [x] (* 2 x))
        g (fn [x] (inc x))

        z (juxt f g)]

    (z 67)))

(defn función-juxt-10
  []
  (let [f (fn [x] (* 2 x))
        g (fn [x] (mod x 2))

        z (juxt f g)]

    (z 11)))

(defn función-juxt-11
  []
  (let [f (fn [x] (mod x 3))
        g (fn [x] (mod x 2))

        z (juxt f g)]

    (z 3434)))

(defn función-juxt-12
  []
  (let [f (fn [x] (max x 3))
        g (fn [x] (mod x 2))

        z (juxt f g)]

    (z 567)))

(defn función-juxt-13
  []
  (let [f (fn [x] (min x 3))
        g (fn [x] (mod x 2))

        z (juxt f g)]

    (z 123)))

(defn función-juxt-14
  []
  (let [f (fn [x] (mod x 3))
        g (fn [x] (quot x 2))

        z (juxt f g)]

    (z 345)))

(defn función-juxt-15
  []
  (let [f (fn [x] (rem x 3))
        g (fn [x] (quot x 2))

        z (juxt f g)]

    (z 5677)))

(defn función-juxt-16
  []
  (let [f (fn [x] (quot x 3))
        g (fn [x] (rem x 2))

        z (juxt f g)]

    (z 3412)))

(defn función-juxt-17
  []
  (let [f (fn [x] (quot x 3))
        g (fn [x] (rem x 2))
        h (fn [x] (mod x 2))

        z (juxt f g h)]

    (z 45456)))

(defn función-juxt-18
  []
  (let [f (fn [x] (rem x 2))
        g (fn [x] (- x 2))
        h (fn [x] (quot x 2))
        i (fn [x] (inc x))

        z (juxt f g h i)]

    (z 1398)))

(defn función-juxt-19
  []
  (let [f (fn [x] (rem x 3))
        g (fn [x] (min x 5))
        h (fn [x] (max x 2))
        i (fn [x] (rem x 2))
        j (fn [x] (quot x 2))

        z (juxt f g h i j)]

    (z 987978)))

(defn función-juxt-20
  []
  (let [f (fn [x] (str x))
        g (fn [x] (rem x 2))

        z (juxt f g)]

    (z 876876)))

(función-juxt-1)
(función-juxt-2)
(función-juxt-3)
(función-juxt-4)
(función-juxt-5)
(función-juxt-6)
(función-juxt-7)
(función-juxt-8)
(función-juxt-9)
(función-juxt-10)
(función-juxt-11)
(función-juxt-12)
(función-juxt-13)
(función-juxt-14)
(función-juxt-15)
(función-juxt-16)
(función-juxt-17)
(función-juxt-18)
(función-juxt-19)
(función-juxt-20)


(defn función-partial-1
  []
  (let [f (fn[x y] (+ x y))
        
        z(partial f 10)]
    
    (z 10)))

(defn función-partial-2
  []
  (let [f (fn [x y] (* x y))

        z (partial f 10)]

    (z 5)))

(defn función-partial-3
  []
  (let [f (fn [x y] (/ x y))

        z (partial f 10)]

    (z 1/7)))

(defn función-partial-4
  []
  (let [f (fn [x y] (vector x y))

        z (partial f 10)]

    (z 23)))

(defn función-partial-5
  []
  (let [f (fn [x y] (list x y))

        z (partial f 10)]

    (z 1/7)))

(defn función-partial-6
  []
  (let [f (fn [x y] (hash-map x y))

        z (partial f 10)]

    (z "1/7")))

(defn función-partial-7
  []
  (let [f (fn [x y] (hash-set x y))

        z (partial f "Hola")]

    (z "Miau")))

(defn función-partial-8
  []
  (let [f (fn [x y] (inc (+ x y)))

        z (partial f 10)]

    (z 15)))

(defn función-partial-9
  []
  (let [f (fn [x y] (dec (+ x y)))

        z (partial f 55)]

    (z 5)))

(defn función-partial-10
  []
  (let [f (fn [x y] (dec (mod x y)))

        z (partial f 55)]

    (z 5)))

(defn función-partial-11
  []
  (let [f (fn [x y] (inc (rem x y)))

        z (partial f 55)]

    (z 5)))

(defn función-partial-12
  []
  (let [f (fn [x y] (dec (quot x y)))

        z (partial f 55)]

    (z 5)))

(defn función-partial-13
  []
  (let [f (fn [x y] (vector (str (+ x y))))

        z (partial f 55)]

    (z 5)))

(defn función-partial-14
  []
  (let [f (fn [x y] (keyword (str (+ x y))))

        z (partial f 55)]

    (z 5)))

(defn función-partial-15
  []
  (let [f (fn [x y] (symbol (str (+ x y))))

        z (partial f 55)]

    (z 5)))

(defn función-partial-16
  []
  (let [f (fn [x y] (vector (str (+ x y)) (str (- x y))))

        z (partial f 55)]

    (z 5)))

(defn función-partial-17
  []
  (let [f (fn [x y] (list (str (+ x y)) (str (- x y))))

        z (partial f 55)]

    (z 10)))


(defn función-partial-18
  []
  (let [f (fn [x y] (vector (max (inc (+ x y)) (inc (- x y)))))

        z (partial f 55)]

    (z 5)))

(defn función-partial-19
  []
  (let [f (fn [x y] (count (vector (str (+ x y)) (str (- x y)))))

        z (partial f 55)]

    (z 5)))

(defn función-partial-20
  []
  (let [f (fn [x y] (first (vector (str (+ x y)) (str (- x y)))))

        z (partial f 55)]

    (z 5)))

(función-partial-1)
(función-partial-2)
(función-partial-3)
(función-partial-4)
(función-partial-5)
(función-partial-6)
(función-partial-7)
(función-partial-8)
(función-partial-9)
(función-partial-10)
(función-partial-11)
(función-partial-12)
(función-partial-13)
(función-partial-14)
(función-partial-15)
(función-partial-16)
(función-partial-17)
(función-partial-18)
(función-partial-19)
(función-partial-20)

(defn función-some-fn-1
  []
  (let [z (some-fn number? int?)]
    
    (z "23" 23)))

(defn función-some-fn-2
  []
  (let [z (some-fn number? float?)]

    (z 23 2.5)))

(defn función-some-fn-3
  []
  (let [z (some-fn number? pos-int?)]

    (z 23 -2.5)))

(defn función-some-fn-4
  []
  (let [z (some-fn number? neg-int?)]

    (z 23 2.5)))

(defn función-some-fn-5
  []
  (let [z (some-fn number? boolean?)]

    (z 23 true)))

(defn función-some-fn-6
  []
  (let [z (some-fn number? ratio?)]

    (z 23 2.5)))

(defn función-some-fn-7
  []
  (let [z (some-fn number? rational?)]

    (z 23 23/6)))

(defn función-some-fn-8
  []
  (let [z (some-fn number? double?)]

    (z 23 2.5)))

(defn función-some-fn-9
  []
  (let [z (some-fn string? char?)]

    (z 23 2.5 "Miau")))

(defn función-some-fn-10
  []
  (let [z (some-fn string? keyword?)]

    (z 23 2.5 :a)))

(defn función-some-fn-11
  []
  (let [z (some-fn symbol? keyword? char?)]

    (z 23 2.5 'a)))

(defn función-some-fn-12
  []
  (let [z (some-fn coll? vector?)]

    (z 23 2.5 {:a [23 23]})))

(defn función-some-fn-13
  []
  (let [z (some-fn coll? list?)]

    (z 23 [2.5] {:a 23})))

(defn función-some-fn-14
  []
  (let [z (some-fn coll? set?)]

    (z 23 2.5 [23 45])))

(defn función-some-fn-15
  []
  (let [z (some-fn coll? associative?)]

    (z 23 [2.5] '("a" 23))))

(defn función-some-fn-16
  []
  (let [z (some-fn coll? map?)]

    (z 23 2.5 [:a 23 :e 34])))

(defn función-some-fn-17
  []
  (let [z (some-fn coll? map? map-entry?)]

    (z 23 2.5 {:a 45})))

(defn función-some-fn-18
  []
  (let [z (some-fn coll? map-entry? indexed?)]

    (z 23 2.5 {} [nil])))

(defn función-some-fn-19
  []
  (let [z (some-fn coll? number? even?)]

    (z 23 2.5 23.1)))

(defn función-some-fn-20
  []
  (let [z (some-fn coll? odd? even?)]

    (z 23 2.5)))

(función-some-fn-1)
(función-some-fn-2)
(función-some-fn-3)
(función-some-fn-4)
(función-some-fn-5)
(función-some-fn-6)
(función-some-fn-7)
(función-some-fn-8)
(función-some-fn-9)
(función-some-fn-10)
(función-some-fn-11)
(función-some-fn-12)
(función-some-fn-13)
(función-some-fn-14)
(función-some-fn-15)
(función-some-fn-16)
(función-some-fn-17)
(función-some-fn-18)
(función-some-fn-19)
(función-some-fn-20)